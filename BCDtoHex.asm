;CALL   scan_num       ; CX.
;CALL   pthis
;DB   'A string', 0
;CALL   print_num      ; AX.
include 'emu8086.inc'
ORG    100h
    jmp start
    value dw ?
    digits dw ?,?,?,?
    result dw 0
    clp dw 4H
    mask dw 000FH
   
start:
;    call pthis
;    db 'input max 4 digit bcd number : ',0
;    call scan_num
   
    mov cx,4596H    
    call BTH
   
    mov ax,result
 
;    GOTOXY 0,1
;    MOV AX,result
   
;    call pthis
;    db 'the hex is stored in AX or in result variable.',0
   
    jmp ending
 
BTH proc near
    ;The source will be in CX
       
    lea di,digits  
    mov value,cx
   
    mov dx,0    
    mov cl,0
   
loop1:
    mov ax,value
    and ax,mask
    ror ax,cl
    mov [di],ax    
    inc di
    inc di
    add cl,4    
   
    mov ax,mask
    mov bx,0010H
    mul bx
    mov mask,ax
    dec clp
       
    cmp clp,0
    ja loop1
   
    ;Nesx
    lea di,digits
    mov bx,1
    mov cx,4
 
loop2:
   
    mov ax,[di]
    mul bx
    add result,ax
       
    mov ax,bx
    mov bx,10
    mul bx
    mov bx,ax
   
    inc di
    inc di
    loop loop2
    ret
   
   
BTH endp
 
 
ending:
 
; macros to define procs
DEFINE_SCAN_NUM
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS  
DEFINE_PTHIS
 
END
